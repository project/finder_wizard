<?php


/**
 * Admin finder wizard list page
 */

function finder_wizard_admin_list() {
  finder_wizard_inc('crud');

  $output = '';
  $finder_wizards = finder_wizard_crud_read_all_summaries();

  if (empty($finder_wizards)) {
    $output .= t("!add finder wizard.", array('!add' => l('Add', 'admin/build/finder_wizard/add')));
  }
  else {
    foreach ((array)$finder_wizards as $finder_wizard) {
      $rows = array();
      $rows[] = array(
        array(
          'data' => "<strong>". $finder_wizard->title ."</strong>", 
          'class' => 'finder-wizard-title',
        ), 
        array(
          'data' => l('Edit', 'admin/build/finder_wizard/edit/'. $finder_wizard->fwid)
            . t(" | ") . l('Delete', 'admin/build/finder_wizard/delete/'. $finder_wizard->fwid), 
          'class' => 'finder-wizard-ops',
          'align' => 'right',
        ), 
      );
      $rows[] = array(
        array(
          'data' => "Path: <span>". l($finder_wizard->path, $finder_wizard->path) ."</span>", 
          'class' => 'finder-wizard-summary',
        ), 
        array(
          'data' => "", 
          'class' => 'finder-wizard-desc description',
        ), 
      );
      $output .= theme('table', array(), $rows, array('class' => 'finder-wizard-table finder-wizard-'. $finder_wizard->fwid));
    }
  }

  return $output;

}

/**
 * Admin finder wizard add/edit page
 */
function finder_wizard_admin_edit($form_state, $finder_wizard = 'add') {
  finder_wizard_inc('node');  // for the 'types' options
  $types = finder_wizard_get_types();
  $field_options = finder_wizard_get_fields();
  // build the form
  $form = array();
  $form['#tree'] = TRUE;
  $form['fwid'] = array(
    '#type' => 'value',
    '#value' => $finder_wizard->fwid ? $finder_wizard->fwid : 'add',
    '#weight' => 0,
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $finder_wizard->title ? $finder_wizard->title : '',
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => 2,
  );
  $form['path'] = array(   //to do : add validation to ensure this is a real path
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $finder_wizard->path ? $finder_wizard->path : '',
    '#description' => t('The finder wizard needs a path from which it can be accessed.'),
    '#size' => 30,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#weight' => 2,
  );
  $form['types'] = array(
    '#type' => 'select',
    '#title' => t('Node types'),
    '#default_value' => $finder_wizard->types ? $finder_wizard->types : array(''),
    '#options' => finder_wizard_get_types(),
    '#description' => t('Pick the node types that will be searched upon, choose &lt;none&gt; for all node types.'),
    '#multiple' => TRUE,
    '#weight' => 4,
    '#size' => 6,
  );
  if (is_numeric($finder_wizard->fwid)) {
    $form['steps'] = array(
      '#title' => t('Steps'),
      '#weight' => 6,
      '#theme' => 'finder_wizard_admin_edit_steps_table',
      '#suffix' => "<div class='add-step'>". l(t('Add a step'), 'admin/build/finder_wizard/edit/'. $finder_wizard->fwid .'/step/add') ."</div>",
    );
    if (!empty($finder_wizard->steps)) {
      foreach($finder_wizard->steps as $key => $value) {
        $step_output = "<div class='finder-wizard-step'>";
        $step_output .= "<div class='finder-wizard-question'><strong>". check_markup($value['question']) ."</strong></div>";
        $step_output .= "<div class='finder-wizard-field'><em>Answers:</em> <span>";
        $step_output .= $field_options[$value['field_handler'] .'.'. $value['field_name']] ."</span></div>";
        $step_output .= "<div class='finder-wizard-question'><em>Response:</em> ". check_markup($value['response']) ."</div>";
        $step_output .= "</div>";
        $form['steps']['info'][$key]['value']['#value'] = $step_output;
        $form['steps']['info'][$key]['ops']['#value'] = 
          l('Edit', 'admin/build/finder_wizard/edit/'. $finder_wizard->fwid . '/step/edit/'. $value['fwsid'])
            . t(" | ") . l('Delete', 'admin/build/finder_wizard/edit/'. $finder_wizard->fwid . '/step/delete/'. $value['fwsid']);
        $form['steps']['info'][$key]['weight'] = array(
          '#type' => 'weight',
          '#delta' => count($finder_wizard->steps),
          '#default_value' => $key,
        );
      }
    }
  }
  else {
    drupal_set_message('You may configure steps for this finder wizard after saving.', 'warning');
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save finder wizard'),
    '#weight' => 8,
  );
  return $form;
}

/**
 * Submit function for admin finder wizard add/edit page
 */
function finder_wizard_admin_edit_submit($form, &$form_state) {
  finder_wizard_inc('crud');
  $values = &$form_state['values'];
  if (!is_numeric($values['fwid'])) {
    $values['fwid'] = finder_wizard_crud_create_summary($values['title'], $values['path']);
  }
  else {
    finder_wizard_crud_update_summary($values['fwid'], $values['title'], $values['path']);
  }
  unset($values['types']['']);
  if (!empty($values['types'])) {
    finder_wizard_crud_update_types($values['fwid'], $values['types']);
  }
  drupal_set_message("The finder wizard was saved.");
  menu_rebuild();
  drupal_goto("admin/build/finder_wizard/edit/". $values['fwid']);
}

/**
 * Admin finder wizard delete page
 */
function finder_wizard_admin_delete($form_state, $finder_wizard) {
  // build the form
  $form = array();
  $form['#tree'] = TRUE;
  $form['fwid'] = array(
    '#type' => 'value', 
    '#value' => $finder_wizard->fwid,
  );
  return confirm_form($form,
    t('Are you sure you want to delete the "!fw" finder wizard?', array('!fw' => $finder_wizard->title)),
    $_GET['destination'] ? $_GET['destination'] : 'admin/build/finder_wizard/list',
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
}

/**
 * Submit function for admin finder wizard delete page
 */
function finder_wizard_admin_delete_submit($form, &$form_state) {
  finder_wizard_inc('crud');
  $fwid = &$form_state['values']['fwid'];
  finder_wizard_crud_delete_summary($fwid);
  finder_wizard_crud_delete_types($fwid);
  finder_wizard_crud_delete_all_steps($fwid);
  drupal_set_message("The finder wizard was deleted.");
  menu_rebuild();
  drupal_goto("admin/build/finder_wizard/list");
}

/**
 * Admin finder wizard step add/edit page
 * must have a Finder wizard object at this point
 */
function finder_wizard_admin_step_edit($form_state, $finder_wizard, $fwsid = 'add') {
  $field_options = finder_wizard_get_fields();

  // because we are not keying by $fwsid, we have to find the correct step like this.
  foreach ($finder_wizard->steps as $key => $value) {
    if ($value['fwsid'] == $fwsid) {
      $step = $finder_wizard->steps[$key];
      break;
    }
  }

  $form = array();
  $form['#tree'] = TRUE;
  $form['fwid'] = array(
    '#type' => 'value',
    '#value' => $finder_wizard->fwid,
    '#weight' => 0,
  );
  $form['fwsid'] = array(
    '#type' => 'value',
    '#value' => $fwsid,
    '#weight' => 2,
  );
  $form['question'] = array(
    '#type' => 'textarea',
    '#title' => t('Question'),
    '#default_value' => $step['question'] ? $step['question'] : '',
    '#required' => TRUE,
    '#size' => 8,
    '#description' => t("The question to ask of the user in this step."),
    '#weight' => 4,
  );
  $form['field'] = array(
    '#type' => 'select',
    '#title' => t('Answers'),
    '#default_value' => ($step['field_handler'] && $step['field_name']) ? $step['field_handler'].'.'.$step['field_name'] : '',
    '#options' => $field_options,
    '#description' => t('Which field to get the answers from.'),
    '#weight' => 6,
  );
  $form['response'] = array(
    '#type' => 'textarea',
    '#title' => t('Response'),
    '#default_value' => $step['response'] ? $step['response'] : 'You have selected !answer',
    '#required' => TRUE,
    '#size' => 8,
    '#description' => t("The message to give the user on the NEXT step.  Use <em>!answer</em> to insert the chosen value."),
    '#weight' => 8,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save finder wizard step'),
    '#weight' => 10,
  );
  return $form;
}

/**
 * Submit function for admin finder wizard add/edit page
 */
function finder_wizard_admin_step_edit_submit($form, &$form_state) {
  finder_wizard_inc('crud');
  $values = &$form_state['values'];

  // expand 'field' values into handler and name
  $field = explode('.', $values['field']);
  $values['field_handler'] = &$field[0];
  $values['field_name'] = &$field[1];

  if (!is_numeric($values['fwsid'])) {
    $values['fwsid'] = finder_wizard_crud_create_step(
      $values['fwid'], $values['question'], $values['response'], $values['field_handler'], 
      $values['field_name']);
  }
  else {
    finder_wizard_crud_update_step(
      $values['fwsid'], $values['fwid'], $values['question'], $values['response'], 
      $values['field_handler'], $values['field_name']);
  }
  drupal_set_message("The finder wizard step was saved.");
  drupal_goto("admin/build/finder_wizard/edit/". $values['fwid']);
}

/**
 * Admin finder wizard delete page
 */
function finder_wizard_admin_step_delete($form_state, $finder_wizard, $fwsid) {
  // build the form
  $form = array();
  $form['#tree'] = TRUE;
  $form['fwsid'] = array(
    '#type' => 'value', 
    '#value' => $fwsid,
  );
  $form['fwid'] = array(
    '#type' => 'value', 
    '#value' => $finder_wizard->fwid,
  );
  return confirm_form($form,
    t('Are you sure you want to delete the finder wizard step?'),
    $_GET['destination'] ? $_GET['destination'] : 'admin/build/finder_wizard/edit/'. $finder_wizard->fwid,
    t('This action cannot be undone.'),
    t('Delete'), t('Cancel')
  );
}

/**
 * Submit function for admin finder wizard delete page
 */
function finder_wizard_admin_step_delete_submit($form, &$form_state) {
  finder_wizard_inc('crud');
  $fwsid = &$form_state['values']['fwsid'];
  $fwid = &$form_state['values']['fwid'];
  finder_wizard_crud_delete_step($fwsid);
  drupal_set_message("The finder wizard step was deleted.");
  drupal_goto("admin/build/finder_wizard/edit/". $fwid);
}

// TO DO:
// validation
// checks to see if database queries were actually executed
// ensure unique weights when no javascript present